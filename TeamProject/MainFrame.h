#pragma once
#include "IFrame.h"
#include <SFML/Graphics.hpp>
#include <array>

const int frames = 11;

namespace Frames {
	class MainFrame : public IFrame {
		sf::RectangleShape rect;
		sf::Text name;
		std::array<sf::Sprite, frames> pictures;
		std::array<sf::Texture, frames> texs;
		int frameIndex = 0;
		sf::Time lastUpdate = sf::Time::Zero;
		int sizeRate = 100;
	public:
		MainFrame();
		~MainFrame() override;
		FrameType get_type() const override;
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
		void update(const sf::Time& dt) override;
	};
}
