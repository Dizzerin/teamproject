#pragma once

/**
 * @file GlobalContext.h
 * Class definition for the global context
 * It handles retrieving globally important
 * objects such as but not limited to the render
 * window, the rendering engine, or the event handler.
 *
 * @author dakriy
 */

#include <SFML/Graphics/RenderWindow.hpp>
#include "Event.h"
#include "Engine.h"

class GlobalContext
{
private:
	// Engine instance
	static Core::Engine * engine_;

	// Global clock instance
	static sf::Clock * global_clock_;

	// Global render window instance
	static sf::RenderWindow * window_;

	// Global event handler instance
	static Core::EventHandler * event_handler_;
public:
	static sf::Clock * get_clock();
	static Core::Engine * get_engine();
	static sf::RenderWindow * get_window();
	static Core::EventHandler * get_event_handler();

	static void set_clock(sf::Clock *);
	static void set_engine(Core::Engine *);
	static void set_window(sf::RenderWindow *);
	static void set_event_handler(Core::EventHandler *);

	static void clear_clock();
	static void clear_window();
	static void clear_engine();
	static void clear_event_handler();

	static void log_debug(std::string&);
};
