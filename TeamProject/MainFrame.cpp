#include "MainFrame.h"
#include "Utilities.h"
#include "TextFactory.h"
#include "GlobalContext.h"

using namespace Frames;

Frames::MainFrame::MainFrame() : IFrame()
{
	rect.setFillColor(sf::Color(0, 0, 0, 0));
	rect.setOutlineColor(sf::Color::White);
	rect.setOutlineThickness(5);
	rect.setPosition(500, 500);
	rect.setSize(sf::Vector2f(101, 101));

	name = Factory::TextFactory::create_displayable_string("TeamProject!");
	name.setCharacterSize(100);
	name.setFillColor(sf::Color::Red);
	name.setPosition(GlobalContext::get_window()->getSize().x / 8.f, GlobalContext::get_window()->getSize().x / 4.f);

	auto index = 0;
	for (auto & tex : texs)
	{
		char str[34];
		sprintf_s(str, "Resources/Background/frame%d.jpg", index++);
		tex.loadFromFile(str);
	}
	index = 0;

	for (auto & pic : pictures)
	{
		pic.setTexture(texs[index++]);
		pic.scale(window_->getSize().x / (pic.getLocalBounds().width), window_->getSize().y / pic.getLocalBounds().height);
	}
}

MainFrame::~MainFrame() {
}

FrameType MainFrame::get_type() const
{
	return MainFrameType;
}

void MainFrame::update(const sf::Time& dt)
{
	auto time = dt.asSeconds();
	lastUpdate += dt;
	if (lastUpdate.asSeconds() >= 1.f / 29.95f)
	{
		lastUpdate = sf::Time::Zero;
		frameIndex = (frameIndex + 1) % frames;
	}
	rect.rotate(180.f * time);

	auto rectDim = rect.getSize();

	const int maxRectDim = 800;
	const int minRectDim = 100;

	if (rectDim.x < minRectDim) {
		rectDim.x = minRectDim;
		rectDim.y = minRectDim;
	}
	if (rectDim.x > maxRectDim) {
		rectDim.x = maxRectDim;
		rectDim.y = maxRectDim;
	}

	if (rectDim.x == maxRectDim) {
		sizeRate *= -1;
		std::cout << "Reached max size!" << std::endl;
	}
	else if (rectDim.x == minRectDim) {
		sizeRate *= -1;
		std::cout << "Reached min size!" << std::endl;
	}


	std::cout << "sizeRate is: " << sizeRate << std::endl;

	rectDim.x += sizeRate * time;
	rectDim.y += sizeRate * time;

	std::cout << "rectDim.x is: " << rectDim.x << std::endl;
	std::cout << "rectDim.y is: " << rectDim.y << std::endl;

	rect.setSize(rectDim);
	rect.setOrigin(rectDim.x / 2, rectDim.y / 2);

	float thickness = rect.getOutlineThickness();
	
	//thickness += 100 * time;

	rect.setOutlineThickness(thickness);

	rect.rotate(80.f * time);
}

void MainFrame::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(pictures[frameIndex]);
	target.draw(rect);
	target.draw(name);
}
