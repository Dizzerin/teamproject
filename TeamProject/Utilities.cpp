#include "Utilities.h"

unsigned GetSeed(const std::string& seed) {
	return static_cast<unsigned>(std::hash<std::string>{}(seed));
}

void QuitWithError(const char* error, int exit_code)
{
#ifdef _WIN32
	MessageBox(nullptr, error, "Runtime Error", MB_OK | MB_ICONINFORMATION);
#else
#include <iostream>
	std::cerr << "Runtime Error" << std::endl;
#endif//_WIN32
	exit(exit_code);
}

int GetRandomNumberInRange(int min, int max)
{
	return min + rand() % (max - min + 1);
}
