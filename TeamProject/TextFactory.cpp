#include "TextFactory.h"
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>

std::array<sf::Font, Factory::None> Factory::TextFactory::font;

void Factory::TextFactory::apply_text_style(sf::Text& t, TextStyles style)
{
	switch (style)
	{
	case DefaultStyle:
		t.setFillColor(sf::Color::Black);
		break;
	case NoStyle:
	default:
		break;
	}
}

bool Factory::TextFactory::load_assets()
{
	return font[DefaultFont].loadFromFile("Resources/SansForgetica-Regular.otf");
}

sf::Text Factory::TextFactory::create_displayable_string(const char* str, const TextStyles style, FontTypes type)
{
	if (type == None)
		type = DefaultFont;

	sf::Text t(
		str,
		font[type]
	);

	apply_text_style(t, style);

	return t;
}

sf::Text Factory::TextFactory::create_displayable_string(const std::string& str, const TextStyles style, FontTypes type)
{
	if (type == None)
		type = DefaultFont;

	sf::Text t(
		str,
		font[type]
	);

	apply_text_style(t, style);

	return t;
}
