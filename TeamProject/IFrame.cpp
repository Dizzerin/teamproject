#include "IFrame.h"
#include "GlobalContext.h"

Frames::IFrame::IFrame()
{
	window_ = GlobalContext::get_window();
}

bool Frames::operator==(const IFrame& lhs, const IFrame& rhs)
{
	return lhs.get_type() == rhs.get_type();
}

bool Frames::operator!=(const IFrame& lhs, const IFrame& rhs)
{
	return !(lhs == rhs);
}
