#include <SFML/Graphics.hpp>
#include "Utilities.h"
#include "GlobalContext.h"
#include "TextFactory.h"
#include <SFML/Audio.hpp>

bool LoadResources()
{
	// Load Resources Here
	// Load fonts
	if (!Factory::TextFactory::load_assets()) {
		return false;
	}

	return true;
}

int main()
{
	// SEED1
	srand(GetSeed("Load your random seed here I guess."));

	sf::Clock elapsedTime;
	GlobalContext::set_clock(&elapsedTime);

	auto vidmode = sf::VideoMode::getFullscreenModes()[0];
	auto vidstyle = sf::Style::Fullscreen;

	if (!vidmode.isValid())
	{
		vidmode = sf::VideoMode::getDesktopMode();
		vidstyle = sf::Style::Default;
		if (!vidmode.isValid())
		{
			vidmode = sf::VideoMode(100, 100);
			if (!vidmode.isValid())
				QuitWithError("Could not find a compatible Video Mode", EXIT_FAILURE);
		}
	}

	if (!LoadResources())
	{
		QuitWithError("Required resources failed to load", EXIT_FAILURE);
	}

	sf::ContextSettings settings;

	settings.antialiasingLevel = 8;

	// Must be kept alive in the root scope for everything else to be able to access it.
	// It adds itself to the global context.
	Core::EventHandler eventHandler;

	sf::Music music;
	music.setLoop(true);
	music.setVolume(100);
	if (!music.openFromFile("resources/music.flac"))
	{
		std::cout << "There was an error playing the song" << std::endl;
	}
	else
	{
		music.play();
	}


	// Start up main rendering window

	sf::RenderWindow Window(vidmode, APP_NAME, vidstyle, settings);
	GlobalContext::set_window(&Window);

	auto escape_to_exit_hook = GlobalContext::get_event_handler()->add_event_callback([](const sf::Event* e) -> bool {
		if (e->key.code == sf::Keyboard::Key::Escape)
		{
			GlobalContext::get_window()->close();
			return true;
		}
		return false;
	}, sf::Event::KeyPressed);


	Core::Engine engine(&Window, &eventHandler, &elapsedTime);

	const auto ev1handle = eventHandler.add_event_callback([&](const sf::Event* e) -> bool {
		Window.close();
		return true;
	}, sf::Event::Closed);

	while (Window.isOpen())
	{
		engine.Loop();
	}

	eventHandler.unhook_event_callback(ev1handle, sf::Event::Closed);
	GlobalContext::get_event_handler()->unhook_event_callback(escape_to_exit_hook, sf::Event::KeyPressed);

	GlobalContext::clear_window();

	return 0;
}
