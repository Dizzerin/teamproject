#pragma once
#include <SFML/Graphics.hpp>
#include "IUpdateable.h"

namespace Frames
{
	enum FrameType
	{
		MainFrameType,
		Count
	};

	class IFrame : public Interfaces::IUpdateable, public sf::Drawable
	{
	protected:
		sf::RenderWindow * window_;
	public:
		IFrame();
		bool hidden = false;
		bool paused = false;
		virtual ~IFrame() = default;
		virtual FrameType get_type() const = 0;
		friend bool operator==(const IFrame & lhs, const IFrame & rhs);
		friend bool operator!=(const IFrame & lhs, const IFrame & rhs);
	};
}
